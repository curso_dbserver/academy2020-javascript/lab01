function numerosPares(x: number, y: number): number[] {
    let result: number[] = new Array;
    let i = 0;
    for (x < y; i++;) {
        if (x % 2 === 0) {
            result.push(i);
        }
    }
    console.log(result.toString());
    return result;
}
numerosPares(2,6);


/*
3. Escreva uma função min(x,y) que devolve o menor entre dois números. Não use funções auxiliares de
Math. Utilize corretamente a declaração de tipos nos parâmetros e no resultado da função.
*/
function min(x:number, y:number) : number {
    if(x < y) {
        //console.log(`Menor Valor: ${x}`);
        return x;
    } else {
        //console.log(`Menor Valor: ${y}`);
        return y;
    }
}
console.log(`Menor valor ${min(5, 2)}`);


/*
4. Escreva uma função pow(x,y) que calcula o valor de 𝑥𝑦
, ou seja, x elevado a potência y. Assuma que os
valores de x e y são números inteiros não negativos e que 𝑥
0 = 1 para qualquer valor de x. Apresente uma
versão iterativa e uma versão recursiva para a função. Não utilize o operador **. Utilize corretamente a
declaração de tipos nos parâmetros e no resultado da função.
*/
function pow(x: number, y: number) {
    if (x >= 0 && y >= 0) {
        let result = Math.pow(x, y);
        return result;
    } else {
        console.log('Número Negativo');
    }
    if (y === 0) {
        let result = 1;
        return result;
    }
}
console.log(pow(2, 5)); 


/*
5. Escreva uma função toMaiusculaPrimeira(s) que recebe uma string s (assuma qualquer string não vazia) e
retorna a mesma string com a primeira letra em maiúscula. Utilize corretamente a declaração de tipos nos
parâmetros e no resultado da função
*/
function toMaiusculaPrimeira(s:string) : string {
    return s.charAt(0).toUpperCase() + s.slice(1);
}
console.log(toMaiusculaPrimeira('luna'));
